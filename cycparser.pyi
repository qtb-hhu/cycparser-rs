from typing import Dict, List, Optional, Tuple, Set
from dataclasses import dataclass

@dataclass
class ParseCompound:
    id: str
    charge: float
    formula: Dict[str, float]
    compartment: str

@dataclass
class RawMonomer:
    gene: str  # Isn't this Optional[str]?
    database_links: Dict[str, Set[str]]

@dataclass
class RawKineticData:
    km: Dict[str, float]
    kcat: Dict[str, float]

@dataclass
class RawCompound:
    base_id: str
    id: str
    charge: int
    gibbs0: Optional[float]
    smiles: Optional[str]
    name: Optional[str]
    types: List[str]
    formula: Dict[str, float]
    database_links: Dict[str, Set[str]]
    compartment: Optional[str]

@dataclass
class RawReaction:
    base_id: str
    id: str
    name: Optional[str]
    ec: Optional[str]
    gibbs0: float
    types: List[str]
    pathways: Set[str]
    database_links: Dict[str, Set[str]]
    # Later additions
    bounds: Tuple[float, float]
    monomers_annotated: Dict[str, Dict[str, RawMonomer]]
    sequences: Dict[str, str]
    kinetic_data: Dict[str, RawKineticData]
    stoichiometries: Dict[str, float]
    transmembrane: bool
    compartment: str
    transmembrane_compartment: Optional[Tuple[str, str]]
    var: Optional[int]

@dataclass
class RawParseResult:
    compounds: Dict[str, RawCompound]
    enzymes: Dict[str, RawKineticData]
    genes: Dict[str, RawMonomer]
    proteins: Dict[str, Set[str]]
    reactions: List[RawReaction]
    sequences: Dict[str, str]

def parse(path: str) -> RawParseResult:
    pass

def fix_rename_types(
    parse_result: RawParseResult, type_map: Dict[str, str]
) -> RawParseResult:
    pass

def fix_add_important_compounds(
    parse_result: RawParseResult,
    manual_additions: Dict[str, ParseCompound],
) -> RawParseResult:
    pass

def fix_unify_reaction_direction(
    parse_result: RawParseResult,
) -> RawParseResult:
    pass

def fix_kinetic_data(
    parse_result: RawParseResult,
) -> RawParseResult:
    pass

def fix_annotate_monomers(
    parse_result: RawParseResult,
) -> RawParseResult:
    pass

def fix_create_reaction_variants(
    parse_result: RawParseResult,
) -> RawParseResult:
    pass

def fix_create_compartment_variants(
    parse_result: RawParseResult,
    compartment_map: Dict[str, str],
    compartment_suffixes: Dict[str, str],
) -> RawParseResult:
    pass

def fix_set_reaction_stoichiometry(
    parse_result: RawParseResult,
) -> RawParseResult:
    pass

def repair(
    parse_result: RawParseResult,
    compartment_map: Dict[str, str],
    type_map: Dict[str, str],
    manual_additions: Dict[str, ParseCompound],
    compartment_suffixes: Dict[str, str],
) -> RawParseResult:
    pass

def pipeline(
    path: str,
    compartment_map: Dict[str, str],
    type_map: Dict[str, str],
    manual_additions: Dict[str, ParseCompound],
    compartment_suffixes: Dict[str, str],
) -> Tuple[List[RawCompound], List[RawReaction]]:
    pass
