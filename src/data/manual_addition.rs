use pyo3::{self, FromPyObject};
use std::collections::HashMap;

#[derive(Debug, Clone, FromPyObject)]
pub struct ManualAddition {
    #[pyo3(attribute("base_id"))]
    pub base_id: String,
    #[pyo3(attribute("id"))]
    pub id: String,
    #[pyo3(attribute("charge"))]
    pub charge: f64,
    #[pyo3(attribute("formula"))]
    pub formula: HashMap<String, f64>,
    #[pyo3(attribute("compartment"))]
    pub compartment: String,
}
