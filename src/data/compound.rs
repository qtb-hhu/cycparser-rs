use pyo3::pyclass;
use std::collections::{HashMap, HashSet};

#[pyclass]
#[derive(Debug, Clone)]
pub struct ParseCompound {
    #[pyo3(get)]
    pub base_id: String,
    #[pyo3(get)]
    pub id: String,
    #[pyo3(get)]
    pub charge: f64,
    #[pyo3(get)]
    pub gibbs0: f64,
    #[pyo3(get)]
    pub smiles: Option<String>,
    #[pyo3(get)]
    pub name: Option<String>,
    #[pyo3(get)]
    pub types: Vec<String>,
    #[pyo3(get)]
    pub formula: HashMap<String, f64>,
    #[pyo3(get)]
    pub database_links: HashMap<String, HashSet<String>>,
    // Fill in later
    #[pyo3(get)]
    pub compartment: Option<String>,
}

impl Default for ParseCompound {
    fn default() -> Self {
        ParseCompound {
            base_id: "".to_owned(),
            id: "".to_owned(),
            charge: 0.0,
            gibbs0: 0.0,
            smiles: None,
            name: None,
            types: Vec::new(),
            formula: HashMap::new(),
            database_links: HashMap::new(),
            compartment: None,
        }
    }
}
