use pyo3::pyclass;
use std::collections::{HashMap, HashSet};

#[pyclass]
#[derive(Debug, Clone)]
pub struct Monomer {
    #[pyo3(get)]
    pub gene: Option<String>,
    #[pyo3(get)]
    pub database_links: HashMap<String, HashSet<String>>,
}

#[pyclass]
#[derive(Debug, Clone)]
pub struct KineticData {
    #[pyo3(get)]
    pub km: HashMap<String, f64>,
    #[pyo3(get)]
    pub kcat: HashMap<String, f64>,
}
