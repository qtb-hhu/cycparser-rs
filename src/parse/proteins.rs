use crate::utils::read_lines;
use std::collections::{HashMap, HashSet};

pub fn parse_proteins(path: &str) -> HashMap<String, HashSet<String>> {
    let mut proteins = HashMap::new();
    let mut current_id = "".to_owned();
    if let Ok(lines) = read_lines(path) {
        for line in lines.into_iter().flatten() {
            if line.starts_with("//") {
                continue;
            }
            let (identifier, content) = line.split_once(" - ").unwrap_or_default();
            match identifier {
                "UNIQUE-ID" => {
                    current_id = content.to_owned();
                }
                "COMPONENTS" => {
                    proteins
                        .entry(current_id.clone())
                        .or_insert_with(HashSet::new)
                        .insert(content.to_owned());
                }
                _ => continue,
            }
        }
    } else {
        eprintln!("Could not find file {}", path);
    }
    proteins
}
