use crate::data::ParseResult;
use pyo3::prelude::*;

use super::utils::reaction_is_good;

#[pyfunction]
pub fn fix_filter_garbage_reactions(mut res: ParseResult) -> ParseResult {
    let compounds = &res.compounds;
    res.reactions
        .retain(|reaction| reaction_is_good(reaction, compounds));
    res
}

impl ParseResult {
    pub fn fix_filter_garbage_reactions(self) -> Self {
        fix_filter_garbage_reactions(self)
    }
}

pub(crate) fn register(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(fix_filter_garbage_reactions, m)?)?;
    Ok(())
}
