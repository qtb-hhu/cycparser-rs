use crate::data::ParseResult;
use pyo3::prelude::*;
use std::collections::HashMap;

fn merge_hashmap(map1: HashMap<String, f64>, map2: HashMap<String, f64>) -> HashMap<String, f64> {
    map1.into_iter().chain(map2).collect()
}

fn remove_duplicates(
    substrates: HashMap<String, f64>,
    products: HashMap<String, f64>,
) -> (HashMap<String, f64>, HashMap<String, f64>) {
    let mut substrates = substrates;
    let mut products = products;
    let mut intersection: Vec<String> = Vec::new();

    for compound in substrates.keys() {
        if products.contains_key(compound) {
            intersection.push(compound.to_owned());
        }
    }
    for compound in intersection.iter() {
        let diff = substrates.get(compound).unwrap() + products.get(compound).unwrap();
        if diff == 0.0 {
            substrates.remove(compound);
            products.remove(compound);
        } else if diff < 0.0 {
            substrates.insert(compound.to_owned(), diff);
            products.remove(compound);
        } else {
            substrates.remove(compound);
            products.insert(compound.to_owned(), diff);
        }
    }
    (substrates, products)
}

#[pyfunction]
pub fn fix_set_reaction_stoichiometry(mut res: ParseResult) -> ParseResult {
    for reaction in res.reactions.iter_mut() {
        // Check for duplicates
        let (substrates, products) =
            remove_duplicates(reaction.substrates.clone(), reaction.products.clone());

        // Create stoichiometry
        reaction.stoichiometries = merge_hashmap(substrates, products);
    }
    res.reactions
        .retain(|reaction| reaction.stoichiometries.len() > 1);
    res
}

impl ParseResult {
    pub fn fix_set_reaction_stoichiometry(self) -> Self {
        fix_set_reaction_stoichiometry(self)
    }
}

pub(crate) fn register(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(fix_set_reaction_stoichiometry, m)?)?;
    Ok(())
}
