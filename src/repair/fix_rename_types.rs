use crate::data::ParseResult;
use pyo3::prelude::*;
use std::collections::HashMap;

fn map_vector(vec: &[String], map: &HashMap<String, String>) -> Vec<String> {
    vec.iter()
        .map(|k| map.get(k).unwrap_or(k).clone())
        .collect()
}

fn map_map<T>(x: &HashMap<String, T>, map: &HashMap<String, String>) -> HashMap<String, T>
where
    T: Clone,
{
    x.iter()
        .map(|(k, v)| (map.get(k).unwrap_or(k).clone(), v.clone()))
        .collect()
}

#[pyfunction]
pub fn fix_rename_types(mut res: ParseResult, type_map: HashMap<String, String>) -> ParseResult {
    for compound in res.compounds.values_mut() {
        compound.types = map_vector(&compound.types, &type_map);
    }
    for reaction in res.reactions.iter_mut() {
        reaction.types = map_vector(&reaction.types, &type_map);
        reaction.substrates = map_map(&reaction.substrates, &type_map);
        reaction.substrate_compartments = reaction
            .substrate_compartments
            .iter()
            .map(|(k, v)| (type_map.get(k).unwrap_or(k).clone(), v.clone()))
            .collect();
        reaction.products = map_map(&reaction.products, &type_map);
        reaction.product_compartments = reaction
            .product_compartments
            .iter()
            .map(|(k, v)| (type_map.get(k).unwrap_or(k).clone(), v.clone()))
            .collect();
    }
    res
}

impl ParseResult {
    pub fn fix_rename_types(self, type_map: HashMap<String, String>) -> Self {
        fix_rename_types(self, type_map)
    }
}

pub(crate) fn register(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(fix_rename_types, m)?)?;
    Ok(())
}
