use crate::data::{ParseReaction, ParseResult};
use pyo3::prelude::*;

fn reverse_stoichiometry(reaction: &mut ParseReaction) {
    let products = reaction
        .substrates
        .iter()
        .map(|(k, v)| (k.to_owned(), -v))
        .collect();
    let substrates = reaction
        .substrates
        .iter()
        .map(|(k, v)| (k.to_owned(), -v))
        .collect();
    let product_compartments = reaction.substrate_compartments.clone();
    let substrate_compartments = reaction.product_compartments.clone();
    reaction.substrates = products;
    reaction.products = substrates;
    reaction.substrate_compartments = product_compartments;
    reaction.product_compartments = substrate_compartments;
    reaction.gibbs0 = -reaction.gibbs0;
}

#[pyfunction]
pub fn fix_unify_reaction_direction(mut res: ParseResult) -> ParseResult {
    let mut reactions = res.reactions;
    for reaction in reactions.iter_mut() {
        if reaction.reversible {
            reaction.bounds = (-1000, 1000);
        } else {
            match reaction.direction.as_str() {
                "LEFT-TO-RIGHT" | "PHYSIOL-LEFT-TO-RIGHT" | "IRREVERSIBLE-LEFT-TO-RIGHT" => {
                    reaction.bounds = (0, 1000);
                }
                "RIGHT-TO-LEFT" | "PHYSIOL-RIGHT-TO-LEFT" | "IRREVERSIBLE-RIGHT-TO-LEFT" => {
                    reverse_stoichiometry(reaction);
                    reaction.bounds = (0, 1000);
                }
                _ => {
                    eprintln!(
                        "Weird reaction direction {} for reaction {}, setting LEFT-TO-RIGHT",
                        reaction.direction, reaction.id,
                    );
                    reaction.bounds = (0, 1000);
                }
            }
        }
    }
    res.reactions = reactions;
    res
}

impl ParseResult {
    pub fn fix_unify_reaction_direction(self) -> Self {
        fix_unify_reaction_direction(self)
    }
}

pub(crate) fn register(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(fix_unify_reaction_direction, m)?)?;
    Ok(())
}
