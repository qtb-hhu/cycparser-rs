#[macro_use]
extern crate maplit;
extern crate pyo3;

use pyo3::prelude::*;

mod data;
mod parse;
mod repair;
mod utils;

use std::{collections::HashMap, path::Path};

use data::{ManualAddition, ParseCompound, ParseReaction, ParseResult};
use parse::{
    parse_compounds, parse_enzymes, parse_genes, parse_proteins, parse_reactions, parse_sequences,
};

#[pyfunction]
#[pyo3(name = "parse")]
pub fn parse(path: &str) -> ParseResult {
    let path = Path::new(path);
    ParseResult {
        compounds: parse_compounds(path.join("compounds.dat").to_str().unwrap()),
        enzymes: parse_enzymes(path.join("enzrxns.dat").to_str().unwrap()),
        genes: parse_genes(path.join("genes.dat").to_str().unwrap()),
        proteins: parse_proteins(path.join("proteins.dat").to_str().unwrap()),
        reactions: parse_reactions(path.join("reactions.dat").to_str().unwrap()),
        sequences: parse_sequences(path.join("protseq.fsa").to_str().unwrap()),
    }
}

#[pyfunction]
#[pyo3(name = "repair")]
pub fn repair(
    parse_result: ParseResult,
    compartment_map: HashMap<String, String>,
    type_map: HashMap<String, String>,
    manual_additions: HashMap<String, ManualAddition>,
    compartment_suffixes: HashMap<String, String>,
) -> ParseResult {
    parse_result
        .fix_rename_types(type_map)
        .fix_add_important_compounds(manual_additions)
        .fix_unify_reaction_direction()
        .fix_kinetic_data()
        .fix_annotate_monomers()
        .fix_create_reaction_variants()
        .fix_create_compartment_variants(compartment_map, compartment_suffixes)
        .fix_set_reaction_stoichiometry()

    // Repair
    // fix_filter_garbage_reactions(&mut reactions, &compounds);
}

#[pyfunction]
#[pyo3(name = "pipeline")]
fn pipeline(
    path: &str,
    compartment_map: HashMap<String, String>,
    type_map: HashMap<String, String>,
    manual_additions: HashMap<String, ManualAddition>,
    compartment_suffixes: HashMap<String, String>,
) -> (Vec<ParseCompound>, Vec<ParseReaction>) {
    let parse_results = repair(
        parse(path),
        compartment_map,
        type_map,
        manual_additions,
        compartment_suffixes,
    );

    // Unpack
    let compounds = parse_results.compounds;
    let reactions = parse_results.reactions;
    (compounds.into_values().collect(), reactions)
}

#[pymodule]
fn cycparser(py: Python<'_>, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(parse, m)?)?;
    m.add_function(wrap_pyfunction!(repair, m)?)?;
    m.add_function(wrap_pyfunction!(pipeline, m)?)?;
    repair::fix_add_important_compounds::register(py, m)?;
    repair::fix_annotate_monomers::register(py, m)?;
    repair::fix_create_compartment_variants::register(py, m)?;
    repair::fix_create_reaction_variants::register(py, m)?;
    repair::fix_filter_garbage_reactions::register(py, m)?;
    repair::fix_kinetic_data::register(py, m)?;
    repair::fix_rename_types::register(py, m)?;
    repair::fix_set_reaction_stoichiometry::register(py, m)?;
    repair::fix_unify_reaction_direction::register(py, m)?;
    Ok(())
}
